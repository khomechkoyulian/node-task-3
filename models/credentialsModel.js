const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const validateEmail = require('../utils/validateEmail');
const validatePassword = require('../utils/validatePassword');

const credentialsSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Enter your email.'],
		unique: [true, 'This email is already used.'],		
	},
	password: {
		type: String,
		required: [true, 'Enter your password.'],		
	}
});

credentialsSchema.pre('save', async function () {
	this.password = await bcrypt.hash(this.password, 12);
});

const Credentials = mongoose.model('Credentials', credentialsSchema);

module.exports = Credentials;
