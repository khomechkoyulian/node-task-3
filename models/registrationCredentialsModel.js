const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const validatePassword = require('../utils/validatePassword');
const validateEmail = require('../utils/validateEmail');

const registrationCredentialsSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Enter your email.'],
		unique: [true, 'This email is already used.'],
		
	},
	password: {
		type: String,
		required: [true, 'Enter your password.'],		
	},
	role: {
		type: String,
		required: [true, 'Enter your role'],
		enum: ['DRIVER', 'SYSTEM', 'USER', 'SHIPPER'],
		default: 'USER'
	}
});

registrationCredentialsSchema.pre('save', async function () {
	this.password = await bcrypt.hash(this.password, 12);
});

const RegistrationCredentials = mongoose.model(
	'RegistrationCredentials',
	registrationCredentialsSchema
);

module.exports = RegistrationCredentials;
