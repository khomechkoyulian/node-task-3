const mongoose = require('mongoose');

const validateEmail = require('../utils/validateEmail');

const userSchema = new mongoose.Schema({
	role: {
		type: String,
		required: [true, 'Enter your role.'],
		enum: ['DRIVER', 'SYSTEM', 'USER', 'SHIPPER'],
		default: 'USER'
	},
	email: {
		type: String,
		required: [true, 'Enter your email.'],
		unique: true,		
	},
	created_date: {
		type: String,
		default: new Date().toLocaleDateString()
	}
});

const User = mongoose.model('User', userSchema);

module.exports = User;
