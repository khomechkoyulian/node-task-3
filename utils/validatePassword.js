/**
 *
 * @param {string} password
 * @returns {boolean} returns TRUE is password is valid. returns FALSE if password is invalid
 */

module.exports = function (password) {
	return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(password);
};
